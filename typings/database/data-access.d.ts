import { todoList } from "../types/types.js";
export declare function save(path: string, todos: todoList): Promise<void>;
export declare function load(path: string): Promise<todoList>;
export declare function isFileExists(path: string): Promise<void>;
