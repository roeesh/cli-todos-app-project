import { expect } from "chai";
import { updateValidator } from "../src/controller/cli-validator";

describe("The updateValidator module", () => {
    context("updateValidator", () => {
        const todos = [
            {
                id: "gbo02b1dne8",
                complete: false,
                description: "hw 1"
            },
            {
                id: "gbo02b1dne5",
                complete: false,
                description: "hw 2"
            }
        ];

        it("should exist", () => {
            expect(updateValidator).to.be.instanceOf(Function);
        });

        it("should return true for a valid id", () => {
            const actual = updateValidator(["gbo02b1dne8"], todos);
            expect(actual).to.equal(true);
        });

        it("should return true for valid ids", () => {
            const actual = updateValidator(["gbo02b1dne8", "gbo02b1dne5"], todos);
            expect(actual).to.equal(true);
        });

        it("should return false for an invalid id", () => {
            const actual = updateValidator(["gbo02b1dne9"], todos);
            expect(actual).to.equal(false);
        });
    });
});