import { expect } from "chai";
import { isValidId } from "../src/controller/cli-validator";

describe("The isValidId module", () => {
    context("isValidId", () => {
        const todos = [
            {
                id: "gbo02b1dne8",
                complete: false,
                description: "hw 1"
            },
            {
                id: "gbo02b1dne5",
                complete: false,
                description: "hw 2"
            }
        ];

        it("should exist", () => {
            expect(isValidId).to.be.instanceOf(Function);
        });

        it("should return true for a valid id", () => {
            const actual = isValidId(["gbo02b1dne8"], todos, []);
            expect(actual).to.equal(true);
        });

        it("should return false for an invalid id", () => {
            const actual = isValidId(["gbo02b1dne9"], todos, []);
            expect(actual).to.equal(false);
        });  
        
        it("should return false if todos is empty", () => {
            const actual = isValidId(["gbo02b1dne9"], [], []);
            expect(actual).to.equal(false);
        });  
    });
});