import log from "@ajar/marker";

// print to cli
export function print(msg: string): void {
    log.red(msg);
}
