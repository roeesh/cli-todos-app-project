import { execute } from "./controller/cli-controller.js";
import { isFileExists, load } from "./database/data-access.js";

const PATH = "dist/todos.json";

// init()
(async () => {
    // check if todos.json file exists, if not - create one
    await isFileExists(PATH);

    // load todos from todos.json
    const todos = await load(PATH);

    const args: string[] = process.argv.slice(2);

    execute(PATH, args, todos);
})();
