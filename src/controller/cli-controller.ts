import { todoList } from "../types/types.js";
import {
    addTaskHandler,
    showListHandler,
    updateTaskHandler,
    editDescriptionHandler,
    removeTaskHandler,
} from "./todo-controller.js";
import { showHelpWindow, showTasks } from "../view/output-cli.js";
import { save } from "../database/data-access.js";
import { print } from "../utils/utils-cli.js";
import {
    addValidator,
    lsValidator,
    updateValidator,
    editValidator,
    removeValidator,
} from "./cli-validator.js";

export async function execute(
    path: string,
    args: string[],
    todos: todoList
): Promise<void> {
    const operation = args[0];
    let isValidCommand: boolean;

    switch (operation) {
        case "add":
            isValidCommand = addValidator(args.slice(1));
            if (!isValidCommand) {
                print("Not a valid input, try again.");
                return showHelpWindow();
            }
            // create a new task
            todos = addTaskHandler(args.slice(1), todos);
            await save(path, todos);
            showTasks(todos);
            break;

        case "ls":
            isValidCommand = lsValidator(args.slice(1));
            if (!isValidCommand) {
                print("Not a valid input, try again.");
                return showHelpWindow();
            }
            // read a list of tasks
            todos = showListHandler(args.slice(1), todos);
            showTasks(todos);
            break;

        case "update":
            isValidCommand = updateValidator(args.slice(1), todos);
            if (!isValidCommand) {
                print("Not a valid input, try again.");
                return showHelpWindow();
            }
            // update task
            todos = updateTaskHandler(args.slice(1), todos);
            await save(path, todos);
            showTasks(todos);
            break;

        case "edit":
            isValidCommand = editValidator(args.slice(1), todos);
            if (!isValidCommand) {
                print("Not a valid input, try again.");
                return showHelpWindow();
            }
            // edit task's description
            todos = editDescriptionHandler(args.slice(1), todos);
            await save(path, todos);
            showTasks(todos);
            break;

        case "rm":
            isValidCommand = removeValidator(args.slice(1), todos);
            if (!isValidCommand) {
                print("Not a valid input, try again.");
                return showHelpWindow();
            }
            // delete a specific task / or some / or completed tasks
            todos = removeTaskHandler(args.slice(1), todos);
            await save(path, todos);
            showTasks(todos);
            break;

        default:
            // show help window
            showHelpWindow();
            break;
    }
}
