import { todoList } from "../types/types";

export function addValidator(commands: string[]): boolean {
    if (commands.length !== 1) {
        return false;
    }
    return true;
}

export function lsValidator(commands: string[]): boolean {
    if (
        commands.length === 1 &&
        (commands[0] === "-c" || commands[0] === "-a")
    ) {
        return true;
    }
    if (commands.length === 0) {
        return true;
    }
    return false;
}

export function updateValidator(commands: string[], tasks: todoList): boolean {
    return isValidId(commands, tasks, [] as string[]);
}

export function editValidator(commands: string[], tasks: todoList): boolean {
    return (
        commands.length === 2 && isValidId([commands[0]], tasks, [] as string[])
    );
}

export function removeValidator(commands: string[], tasks: todoList): boolean {
    if (commands[0] === "-c" && commands.length === 1) {
        return true;
    }
    return isValidId(commands, tasks, [] as string[]);
}

/**
 *
 * @param tasksId
 * @param tasks
 * @param ids
 * @returns true if all tasksId are in tasks else false
 */
export function isValidId(
    tasksId: string[],
    tasks: todoList,
    ids: string[]
): boolean {
    if (tasks.length === 0) {
        return false;
    }

    tasks.forEach((task) => ids.push(task.id));
    for (const taskId of tasksId) {
        if (!ids.includes(taskId)) {
            return false;
        }
    }
    return true;
}
