import fs from "fs/promises";
import { todoList } from "../types/types.js";

// save
export async function save(path: string, todos: todoList): Promise<void> {
    await fs.writeFile(path, JSON.stringify(todos, null, 2));
}

// load
export async function load(path: string): Promise<todoList> {
    const json = await fs.readFile(path, "utf-8");
    const todos = JSON.parse(json);
    return todos;
}

// check if a file exists, if not - create one
export async function isFileExists(path: string): Promise<void> {
    try {
        await fs.access(path);
    } catch {
        await fs.writeFile(path, JSON.stringify([]), "utf-8");
    }
}
